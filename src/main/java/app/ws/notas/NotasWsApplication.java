package app.ws.notas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotasWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotasWsApplication.class, args);
	}

}
